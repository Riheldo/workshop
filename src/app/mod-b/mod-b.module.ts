import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModBRoutingModule } from './mod-b-routing.module';
import { PagebComponent } from './pageb/pageb.component';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';

@NgModule({
  imports: [
    CommonModule,
    ModBRoutingModule,
    DxDataGridModule
  ],
  declarations: [PagebComponent]
})
export class ModBModule { }
