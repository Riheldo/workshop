import { Component, OnInit } from '@angular/core';
import { garbageb } from './datab-overload';

@Component({
  selector: 'app-pageb',
  templateUrl: './pageb.component.html',
  styles: []
})
export class PagebComponent implements OnInit {

  i: number = 0;
  
  get letter(): string {
    return garbageb[this.i];
  }

  constructor() { }

  ngOnInit() {
  }

}
