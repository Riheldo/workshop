import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagebComponent } from './pageb/pageb.component';

const routes: Routes = [
  {path: "", component: PagebComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModBRoutingModule { }
