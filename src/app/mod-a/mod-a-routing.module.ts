import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageaComponent } from './pagea/pagea.component';

const routes: Routes = [
  { path: '', component: PageaComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class ModARoutingModule { }
