import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModARoutingModule } from './mod-a-routing.module';
import { PageaComponent } from './pagea/pagea.component';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';

@NgModule({
  imports: [
    CommonModule,
    ModARoutingModule,
    DxDataGridModule
  ],
  declarations: [PageaComponent]
})
export class ModAModule { }
