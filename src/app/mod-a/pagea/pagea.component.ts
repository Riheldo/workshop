import { Component, OnInit } from '@angular/core';
import { garbage } from './data-overload';

@Component({
  selector: 'app-pagea',
  templateUrl: './pagea.component.html',
  styles: []
})
export class PageaComponent implements OnInit {

  i: number = 0;
  
  get letter(): string {
    return garbage[this.i];
  }
  
  constructor() { }

  ngOnInit() {
  }

}
