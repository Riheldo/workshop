import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompAComponent } from './comp-a/comp-a.component';
import { Routes, RouterModule } from '../../../node_modules/@angular/router';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';

const routesModC: Routes = [
  { path:"", component: CompAComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routesModC),
    DxDataGridModule,
  ],
  declarations: [
    CompAComponent
  ]
})
export class ModCModule { }
